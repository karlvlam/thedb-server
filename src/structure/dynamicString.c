#include "dynamicString.h"
#include <stdlib.h>
#include <string.h>

#define _1MB 1048576

void dsInit(struct dynamicString *ds, int len) {
  ds->buffer = malloc(len);
  ds->free = 0;
  ds->len = len;
}

void dsSet(struct dynamicString *ds, char *s) {
  int l = strlen(s);
  if (ds->buffer == NULL || l >= ds->len) {
    if (l < _1MB) {
      dsInit(ds, l * 2 + 1);
    } else {
      dsInit(ds, l + _1MB + 1);
    }
  }
  strcpy(ds-> buffer, s);
}
