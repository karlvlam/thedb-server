![](https://www.quantr.foundation/wp-content/uploads/2023/10/TheDB-english-no-HK-wide.png)

TheDB is a nosql database written in C

# Member

Peter, System Architect <peter@quantr.hk>

Miles, Programmer <miles@quantr.hk>

# Dev notes

## VSCode setup

1. install https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.triggertaskonsave
2. edit any files in src, it will run "make" after save

## Project website and all doc

www.quantr.foundation

https://www.quantr.foundation/docs/?project=TheDB
