CFLAGS=-g -Wall

all: bin cli bin/argparse.o bin/socketServer.o bin/thedb-server bin/thedb-client bin/tcpServer

bin:
	mkdir -p bin

cli: src/cli/cli.l src/cli/cli.y
	flex -o bin/lex.yy.c src/cli/cli.l
	bison -d src/cli/cli.y -o bin/cli.tab.c
	gcc -c bin/lex.yy.c $(CFLAGS) -o bin/lex.yy.o
	gcc -c bin/cli.tab.c $(CFLAGS) -o bin/cli.tab.o
	gcc -c bin/cli.tab.c $(CFLAGS) -o bin/cli.tab.o

bin/argparse.o: src/argparse/argparse.c
	gcc -c $? $(CFLAGS) -o $@

bin/socketServer.o: src/socket/socketServer.c
	gcc -c $? $(CFLAGS) -o $@
	
bin/thedb-server: bin/dynamicString.o bin/doubleLinkedList.o src/thedb-server.c 
	gcc -c src/thedb-server.c $(CFLAGS) -o bin/thedb-server.o
	gcc bin/*.o -o $@ -lc -lpthread

bin/doubleLinkedList.o: src/structure/doubleLinkedList.c
	gcc -c -O3 -g $? -o $@

bin/dynamicString.o: src/structure/dynamicString.c
	gcc -c -O3 -g $? -o $@

bin/thedb-client: src/client/thedb-clien:t.c
	gcc $(CFLAGS) $? -o $@

bin/tcpServer: src/tcpServer.c
	gcc $(CFLAGS) $? -o $@

clean:
	rm -fr bin
